import React, { useEffect } from 'react';
import { connect } from 'react-redux';

const C = ({ state, setState }) => {
    useEffect(() => {
        console.log(state);

    }, [state])

    useEffect(() => {
        return function () {
            console.log('dead');
        }
    }, [])
    
    return (
        <input
            value={state}
            onChange={(e) => setState(e.target.value)}
        />
    )
}

const mpStateToProps = (state) => {
    return ({
        state: state.state
    })
}

const mapDispatchToProps = (dispatch) => {
    return ({
        setState: (value) => dispatch({ type: 'UPDATE_STATE', payload: value })
    })
}

export default connect(mpStateToProps, mapDispatchToProps)(C);