import React from 'react';
import { connect } from 'react-redux';

const D = ({ state }) => {
    return (
        <div>
            {state}
        </div>
    )
}

const mapStateToProps = (globalState) => {
    return ({
        state: globalState.state
    });
}

export default connect(mapStateToProps)(D);