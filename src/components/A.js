import React from 'react';

import B from './B';
import E from './E';

const A = () => {
    return (
        <>
            <B />
            <E />
        </>
    )
}

export default A;
