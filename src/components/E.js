import React from 'react';
import { connect } from 'react-redux';
import { createSelector } from 'reselect'

const stateSelector = (globalState) => globalState.state;

const modifiedStateSelector = createSelector(stateSelector, (result) => result.toUpperCase());

const E = ({ state }) => {
    return (
        <div>
            {`The input feild value is ${ state }`}
        </div>
    )
}

const mapStateToProps = (globalState) => {
    return ({
        state: modifiedStateSelector(globalState)
    })
}

export default connect(mapStateToProps)(E);