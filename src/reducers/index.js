const INITIAL_STATE = {
    state: 'Hello from Redux',
    state1: 'Priyanka',
    state2: 'Bhadra'
};

const reducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'UPDATE_STATE':
            return { ...state, state: action.payload }
        case 'UPDATE_STATE1':
            return { ...state, state1: action.payload }
        case 'UPDATE_STATE2':
            return { ...state, state2: action.payload }
        default:
            return state;
    }
};
    
export default reducer;




    //   case 'GET_NEWS_LOADING':
    //     return { ...state, loading: true };
    //   case 'GET_NEWS_SUCCESS':
    //     return { ...state, news: action.payload.articles[0], loading: false }
    //   case 'GET_NEWS_FAILURE':
    //     return { ...state, error: action.payload.response.error, loading: false }