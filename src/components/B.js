import React from 'react';

import C from './C';
import D from './D';
import { connect } from 'react-redux';

const B = ({ state }) => {
  return (
    <>
      {state !== 'kill' && <C />}
      <D />
    </>
  )
}

const mpStateToProps = (state) => {
  return ({
      state: state.state
  })
}

export default connect(mpStateToProps)(B);